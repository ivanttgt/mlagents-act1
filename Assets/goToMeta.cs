﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using System;

public class goToMeta : Agent
{
    int checkpointmultipliyer = 1;

    

    public GameObject[] checkpoints;

    public float spd = 1;

    [SerializeField] private Transform target;

    float dist;
    float lastdist;

    public override void OnEpisodeBegin()
    {
        this.lastdist = Vector3.Distance(this.transform.position, target.position);
        
        this.checkpointmultipliyer = 1;
        transform.localPosition = new Vector3(0, 1.5f, -20);
        this.checkpoints[0].gameObject.SetActive(true);
        this.checkpoints[1].gameObject.SetActive(true);
        this.checkpoints[2].gameObject.SetActive(true);
        SetReward(-150f);

        InvokeRepeating("depresa", 0, 1);
        InvokeRepeating("distancies", 0, 1);

    }

    

    private void depresa()
    {
        SetReward(-1f);
    }

    private void distancies()
    {
        this.dist = Vector3.Distance(this.transform.position, target.position);
        if (this.dist < this.lastdist)
        {
            SetReward(0.5f);
        }
        else if (this.dist > this.lastdist)
        {
            SetReward(-0.5f);
        }
        this.lastdist = this.dist;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);
        sensor.AddObservation(this.checkpoints[0].transform.localPosition);
        sensor.AddObservation(this.checkpoints[1].transform.localPosition);
        sensor.AddObservation(this.checkpoints[2].transform.localPosition);
        sensor.AddObservation(this.checkpoints[3].transform.localPosition);
        sensor.AddObservation(this.checkpoints[4].transform.localPosition);
        sensor.AddObservation(this.lastdist);
        sensor.AddObservation(this.dist);

    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];
        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime * spd;
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> acCont = actionsOut.ContinuousActions;
        acCont[0] = Input.GetAxisRaw("Horizontal");
        acCont[1] = Input.GetAxisRaw("Vertical");
    }

    private void OnCollisionStay(Collision collision)
    {
        this.dist = Vector3.Distance(this.transform.position, target.position);
        if (collision.transform.tag == "limits")
        {
            SetReward(-350);
            EndEpisode();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "meta")
        {
            SetReward(300);
            EndEpisode();
        } else if (other.transform.tag == "checkpoint")
        {
            SetReward(5 * this.checkpointmultipliyer);
            this.checkpointmultipliyer++;
            other.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        
    }

}
